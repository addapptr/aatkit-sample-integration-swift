//
//  Placements.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 07.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import Foundation
import AATKit

typealias AdRequestCompletionHandler = ((UIView?, Error?) -> ())

protocol StateScreen {
    var state: AdRequestState { get }
    func updateView()
}

enum AdRequestState {
    case idle
    case loading
    case loaded
    case loadedWithNoAds
}

enum InfeedBannerState {
    case idle
    case loading
    case failed(Error)
    case loaded(UIView)
}

class Placements {
    static let shared: Placements = Placements()
    init(){}

    var userEarnedIncentive = 0
}
