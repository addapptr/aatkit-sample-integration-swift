//
//  StickyBannerViewController.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 11.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit

class StickyBannerViewController: UIViewController {
    @IBOutlet weak var stickyBannerView: UIView!
    @IBOutlet weak var multiSizeBannerView: UIView!
    
    // Sticky Placement
    var stickyBannerPlacement: AATKitPlacement? {
        get {
            let placementName = "StickyBannerPlacement"
            guard let placement = AATKit.getPlacementWithName(placementName) else {
                return AATKit.createPlacement(withName: placementName, andType: .banner414x53)
            }
            return placement
        }
    }
    
    // Multisize Placement
    var multisizeBannerPlacement: AATKitPlacement? {
        get {
            let placementName = "MultiSizeBanner"
            guard let placement = AATKit.getPlacementWithName(placementName) else {
                return AATKit.createPlacement(withName: placementName, andType: .bannerMultiSize)
            }
            return placement
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        title = "Sticky / Multisize Banners"

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveStickyAd),
                                               name: NSNotification.Name(NotificationsNames.aatKitHaveAd.rawValue),
                                               object: stickyBannerPlacement)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveMultisizeAd),
                                               name: NSNotification.Name(NotificationsNames.aatKitHaveAd.rawValue),
                                               object: multisizeBannerPlacement)
        if let stickyPlm = stickyBannerPlacement {
            AATKit.startPlacementAutoReload(withSeconds: 20, for: stickyPlm)
        }
        if let multiSizePlm = multisizeBannerPlacement {
            AATKit.startPlacementAutoReload(withSeconds: 20, for: multiSizePlm)
        }
    }
    
    deinit {
        if let stickyPlm = stickyBannerPlacement {
            AATKit.stopPlacementAutoReload(stickyPlm)
        }
        if let multiSizePlm = multisizeBannerPlacement {
            AATKit.stopPlacementAutoReload(multiSizePlm)
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didReceiveStickyAd() {
        if let stickyPlm = stickyBannerPlacement,
            let bannerView = AATKit.getPlacementView(stickyPlm) {
            displayStickyBanner(bannerView: bannerView)
        }
    }
    
    @objc func didReceiveMultisizeAd() {
        if let multiSizePlm = multisizeBannerPlacement,
           let bannerView = AATKit.getPlacementView(multiSizePlm) {
            displayMultiSizeBanner(bannerView: bannerView)
        }
    }
    
    func displayMultiSizeBanner(bannerView: UIView) {
        removeSubviewsFor(parentView: multiSizeBannerView)
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: bannerView.frame.height)
        bannerView.frame = frame
        multiSizeBannerView.addSubview(bannerView)
    }
    
    func displayStickyBanner(bannerView: UIView) {
        removeSubviewsFor(parentView: stickyBannerView)
        let frame = CGRect(x: 0, y: stickyBannerView.frame.height - 52, width: view.frame.width, height: 52)
        bannerView.frame = frame
        stickyBannerView.addSubview(bannerView)
    }
    
    func removeSubviewsFor(parentView: UIView) {
        parentView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
    }
}
