//
//  EmbeddedAdTableViewCell.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 11.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import UIKit

class EmbeddedAdTableViewCell: UITableViewCell {
    
    
    var adView: UIView? {
        didSet {
            setAdViewConstraints()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setAdViewConstraints() {
        contentView.subviews.forEach { (view) in
            if !view.isKind(of: UIButton.self) {
                view.removeFromSuperview()
            }
        }
        guard let adView = adView else {
            return
        }
        contentView.addSubview(adView)
        
        adView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        adView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        adView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0).isActive = true
        adView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0).isActive = true
        
    }
}
