//
//  InFeedBannerViewController.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 11.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit

class InFeedBannerViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var firstBannerView: UIView?
    var secondBannerView: UIView?
    
    var dataSource: [InfeedBannerState] = [InfeedBannerState](repeating: .idle, count: 8)
    
    var inFeedBannerPlacement: AATBannerPlacementProtocol? {
        get {
            let placementName = PlacementsNames.InFeedBannerPlacement.rawValue
            guard let placement = AATKit.getPlacementWithName(placementName) as? AATBannerPlacementProtocol else {
                return AATKit.createBannerPlacement(name: placementName)
            }
            return placement
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        title = "InFeed Banner"
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadBanners()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cancelAllRequests()
    }
    
    func cancelAllRequests() {
        for (index, _) in dataSource.enumerated() {
            dataSource[index] = .idle
        }
    }
    
    func setupTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.reloadData()
    }
    
    func loadBanners() {
        for (index, state) in dataSource.enumerated() {
            dataSource[index] = .loading
            let indexPath = IndexPath(row: index, section: 0)
            let request = AATAdRequest(viewController: self)

            inFeedBannerPlacement?.execute(request) { [weak self] (adView, error) in
                defer {
                    self?.tableView.beginUpdates()
                    self?.tableView.reloadRows(at: [indexPath], with: .automatic)
                    self?.tableView.endUpdates()
                }

                guard error == nil,
                    let view = adView else {
                        //Handle Error
                        self?.dataSource[index] = .failed(error!)
                        return
                }
                
                self?.dataSource[index] = .loaded(view)
            }
            
        }
    }
}

extension InFeedBannerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return bannerCell(indexPath: indexPath, state: dataSource[indexPath.row])
    }
    
    func bannerCell(indexPath: IndexPath, state: InfeedBannerState) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellsIdentifiers.embeddedBannerAd,
                                                       for: indexPath) as? EmbeddedAdTableViewCell else {
            return UITableViewCell()
        }
        switch dataSource[indexPath.row] {
        
        case .idle:
            cell.textLabel?.text = "idle"
        case .loading:
            cell.textLabel?.text = "loading"
        case .failed(let error):
            cell.textLabel?.text = error.localizedDescription
        case .loaded(let adView):
            cell.adView = adView
        }
        return cell
    }
}
