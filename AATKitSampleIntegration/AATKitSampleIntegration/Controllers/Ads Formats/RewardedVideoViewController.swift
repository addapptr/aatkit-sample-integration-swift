//
//  RewardedVideoViewController.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 12.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit

class RewardedVideoViewController: UIViewController, StateScreen {
    
    @IBOutlet weak var loadButton: UIButton!
    @IBOutlet weak var earnedIncentiveLabel: UILabel!
    
    var state: AdRequestState = .idle {
        didSet {
            updateView()
        }
    }
    
    var rewardedVideoPlacement: AATKitPlacement? {
        get {
            let placementName = PlacementsNames.RewardedVideoPlacement1.rawValue
            guard let placement = AATKit.getPlacementWithName(placementName) else {
                return AATKit.createRewardedVideoPlacement(placementName)
            }
            return placement
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        title = "Rewarded Video"
        updateCoinsLabel()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveAd),
                                               name: NSNotification.Name(NotificationsNames.aatKitHaveAd.rawValue),
                                               object: rewardedVideoPlacement)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userEarnedIncentive),
                                               name: NSNotification.Name(NotificationsNames.aatKitUserEarnedIncentive.rawValue),
                                               object: rewardedVideoPlacement)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(loadedWithNoAds),
                                               name: NSNotification.Name(NotificationsNames.aatKitNoAds.rawValue),
                                               object: rewardedVideoPlacement)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        guard let placement = rewardedVideoPlacement else {
            return
        }
        AATKit.stopPlacementAutoReload(placement)
    }
        
    @objc func didReceiveAd() {
        state = .loaded
    }
    
    @objc func loadedWithNoAds() {
        state = .loadedWithNoAds
    }
    
    func updateView() {
        switch state {
        case .idle:
            loadButton.setTitle("Load Rewarded Video", for: .normal)
            loadButton.isEnabled = true
        case .loading:
            loadButton.setTitle("Loading...", for: .normal)
            loadButton.isEnabled = false
        case .loaded:
            loadButton.setTitle("Show Rewarded Video", for: .normal)
            loadButton.isEnabled = true
        case .loadedWithNoAds:
            loadButton.setTitle("No Ads, Try Again", for: .normal)
            loadButton.isEnabled = true
        }
    }

    func setupRewardedVideo() {
        state = .loading
        guard let placement = rewardedVideoPlacement else {
            return
        }
        AATKit.setPlacementViewController(self, for: placement)
        AATKit.stopPlacementAutoReload(placement)
        AATKit.reload(placement, forceLoad: true)
    }

    @IBAction func loadButtonAction(_ sender: Any) {
        switch state {
        case .idle, .loadedWithNoAds:
            setupRewardedVideo()
        case .loading:
            print("Loading")
        case .loaded:
            showRewardedVideo()
        }
    }
    
    func showRewardedVideo() {
        guard let placement = rewardedVideoPlacement else {
            return
        }
        let canDisplayFullScreenAd = AATKit.show(placement)
        if !canDisplayFullScreenAd {
            state = .loadedWithNoAds
            print("👽👽👽 No Rewarded Video")
        } else {
            state = .idle
        }
    }
    
    @objc func userEarnedIncentive() {
        Placements.shared.userEarnedIncentive += 50
        updateCoinsLabel()
    }
    
    func updateCoinsLabel() {
        earnedIncentiveLabel.text = "Points: \(Placements.shared.userEarnedIncentive)"
    }
}

