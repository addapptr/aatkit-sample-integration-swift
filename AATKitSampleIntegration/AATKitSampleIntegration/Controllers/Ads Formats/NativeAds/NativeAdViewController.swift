//
//  NativeAdViewController.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 08.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit
import GoogleMobileAds

class NativeAdViewController: UIViewController, StateScreen {
    @IBOutlet weak var showNativeAdButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var state: AdRequestState = .idle {
        didSet {
            updateView()
        }
    }
    
    var nativeAdPlacement: AATKitPlacement? {
        get {
            let placementName = PlacementsNames.NativeAdPlacement1.rawValue
            guard let placement = AATKit.getPlacementWithName(placementName) else {
                return AATKit.createPlacement(withName: nativeAdPlacementName,
                                              andType: .nativeAd)
            }
            return placement
        }
    }
    let nativeAdPlacementName = PlacementsNames.NativeAdPlacement1.rawValue
    var nativeAd: AATKitNativeAdProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        title = "Native Ad"
                
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(adFound),
                                               name: NSNotification.Name(NotificationsNames.aatKitHaveAd.rawValue),
                                               object: nativeAdPlacement)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(noAdsFound),
                                               name: NSNotification.Name(NotificationsNames.aatKitNoAds.rawValue),
                                               object: nativeAdPlacement)
    }
    
    @objc func noAdsFound() {
        state = .loadedWithNoAds
    }
    
    @objc func adFound() {
        state = .loaded
    }
    
    func loadAdIfNeeded() {
        guard let placement = nativeAdPlacement else {
            return
        }
        AATKit.reload(placement, forceLoad: true)
        state = .loading
    }
    
    func updateView() {
        switch state {
        case .idle:
            showNativeAdButton.setTitle("Load Native Ad", for: .normal)
            showNativeAdButton.isEnabled = true
        case .loading:
            showNativeAdButton.setTitle("Loading...", for: .normal)
            showNativeAdButton.isEnabled = false
        case .loaded:
            showNativeAdButton.setTitle("Show Native Ad", for: .normal)
            showNativeAdButton.isEnabled = true
        case .loadedWithNoAds:
            showNativeAdButton.setTitle("No Ads, Try Again", for: .normal)
            showNativeAdButton.isEnabled = true
        }
    }
    
    @IBAction func loadNativeAd(_ sender: UIButton) {
        switch state {
        case .idle, .loadedWithNoAds:
            loadAdIfNeeded()
        case .loading:
            print("Loading")
        case .loaded:
            showNativeAd()
        }
    }
    
    func showNativeAd() {
        guard let placement = nativeAdPlacement else {
            return
        }
        guard let fetchedNativeAd = AATKit.getNativeAd(for: placement) else {
            showNativeAdButton.setTitle("No Ads! Try Again", for: .normal)
            state = .loadedWithNoAds
            return
        }
        nativeAd = fetchedNativeAd
        var identifier = "NativeAdAssetViewController"
        if isGoogleAd(nativeAd: fetchedNativeAd) {
            print("🛑🛑🛑 Google Native Ad")
            identifier = "GoogleNativeAdAssetViewController"
        }
        showNativeAdFor(storyboardIdentifier: identifier)
        
        state = .idle
    }
    
    func isGoogleAd(nativeAd: AATKitNativeAdProtocol) -> Bool {
        let network = AATKit.getNativeAdNetwork(nativeAd)
        return network == AATKitAdNetwork.AATDFP ||
            network == AATKitAdNetwork.AATAdX ||
            network == AATKitAdNetwork.AATAdMob
    }
    
    func showNativeAdFor(storyboardIdentifier: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as? NativeAdAssetViewController else {
            return
        }
        embedController(controller: controller)
        controller.nativeAd = nativeAd
    }
    
    func embedController(controller: UIViewController) {
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        addChild(controller)
        view.addSubview(controller.view)
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0),
            controller.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0),
            controller.view.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            controller.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0)
        ])
        controller.didMove(toParent: self)
    }  
}
