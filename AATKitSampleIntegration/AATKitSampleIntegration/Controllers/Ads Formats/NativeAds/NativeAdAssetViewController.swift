//
//  NativeAdAssetViewController.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 17.01.20.
//  Copyright © 2020 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit
import GoogleMobileAds

class NativeAdAssetViewController: UIViewController {
    //Outlets
    @IBOutlet weak var nativeAdContainerView: GADNativeAdView!
    @IBOutlet weak var adTitleLabel: UILabel!
    @IBOutlet weak var adIconImageView: UIImageView!
    @IBOutlet weak var adMainImageView: UIImageView!
    @IBOutlet weak var adBodyLabel: UILabel!
    @IBOutlet weak var adIdentifierLabel: UILabel!
    @IBOutlet weak var adCTALabel: UILabel!
    
    var nativeAd: AATKitNativeAdProtocol? {
        didSet {
            updateNativeAd()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func updateNativeAd() {
        guard let nativeAd = nativeAd else {
            return
        }
        // Set ViewController and tracking view for the native ad
        AATKit.setViewController(self, for: nativeAd)
        AATKit.setTrackingView(nativeAdContainerView, for: nativeAd, mainImageView: adMainImageView, iconView: adIconImageView)
        bindNativeAd()
    }
    
    func bindNativeAd() {
        guard let nativeAd = nativeAd else {
            return
        }
        // Ad Title
        adTitleLabel.text = AATKit.getNativeAdTitle(nativeAd) ?? "-"
        // Ad Icon
        loadImage(for: adIconImageView, imageUrlString: AATKit.getNativeAdIconURL(nativeAd))
        // Ad Main Image
        loadImage(for: adMainImageView, imageUrlString: AATKit.getNativeAdImageURL(nativeAd))
        // Ad Body
        adBodyLabel.text = AATKit.getNativeAdDescription(nativeAd) ?? "-"
        // Ad Identifier
        adIdentifierLabel.text = AATKit.getNativeAdAdvertiser(nativeAd) ?? "-"
        // Ad CTA Title
        adCTALabel.text = AATKit.getNativeAdCall(toAction: nativeAd) ?? "-"
    }
    
    func loadImage(for imageView: UIImageView, imageUrlString: String?) {
        guard let urlString = imageUrlString,
            let url = URL(string: urlString) else {
                return
        }
        load(url: url, imageView: imageView)
    }
    
    func load(url: URL, imageView: UIImageView) {
        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url),
                let image = UIImage(data: data) else {
                return
            }
            DispatchQueue.main.async {
                imageView.image = image
            }
        }
    }
}
