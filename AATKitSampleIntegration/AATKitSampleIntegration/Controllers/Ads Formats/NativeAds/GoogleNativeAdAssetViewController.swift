//
//  GoogleNativeAdAssetViewController.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 17.01.20.
//  Copyright © 2020 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit
import GoogleMobileAds

class GoogleNativeAdAssetViewController: NativeAdAssetViewController {
    @IBOutlet weak var brandingLogoView: UIView!
    
    var googleMediaView: GADMediaView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func updateNativeAd() {
        guard let nativeAd = nativeAd else {
            return
        }
        // Set ViewController and tracking view for the native ad
        AATKit.setViewController(self, for: nativeAd)
        AATKit.setTrackingView(nativeAdContainerView, for: nativeAd, mainImageView: adMainImageView, iconView: adIconImageView)
        bindNativeAd()
        setupGoogleAd()
    }
    
    func setupGoogleAd() {
        // Title
        nativeAdContainerView.headlineView = adTitleLabel
        // Body
        nativeAdContainerView.bodyView = adBodyLabel
        //Media View
        updateMediaView()
        // CTA
        nativeAdContainerView.callToActionView = adCTALabel
        // Branding View
        updateBrandingView()
        // Icon
        nativeAdContainerView.iconView = adIconImageView
    }
    
    func updateBrandingView() {
        guard let nativeAd = nativeAd,
            let brandingImageView = AATKit.getNativeAdBrandingLogoView(nativeAd) else {
                return
        }
        brandingLogoView.frame = brandingLogoView.bounds
        brandingLogoView.addSubview(brandingImageView)
    }

    func updateMediaView() {
        googleMediaView?.removeFromSuperview()
        googleMediaView = nil
        adMainImageView.image = nil
        googleMediaView = GADMediaView(frame: adMainImageView.bounds)
        nativeAdContainerView.mediaView = googleMediaView
        guard let googleMediaView = googleMediaView else {
            return
        }
        adMainImageView.addSubview(googleMediaView)
    }
}
