//
//  InterstitialViewController.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 11.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit

class InterstitialViewController: UIViewController, StateScreen {

    @IBOutlet weak var loadButton: UIButton!
    
    var interstitialPlacement: AATKitPlacement? {
        get {
            let placementName = PlacementsNames.InterstitialPlacement.rawValue
            guard let placement = AATKit.getPlacementWithName(placementName) else {
                return AATKit.createPlacement(withName: PlacementsNames.InterstitialPlacement.rawValue,
                                              andType: .fullscreen)
            }
            return placement
        }
    }
    var state: AdRequestState = .idle {
        didSet {
            updateView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        title = "Interstitial"
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didReceiveAd),
                                               name: NSNotification.Name(NotificationsNames.aatKitHaveAd.rawValue),
                                               object: interstitialPlacement)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(loadedWithNoAds),
                                               name: NSNotification.Name(NotificationsNames.aatKitNoAds.rawValue),
                                               object: interstitialPlacement)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        guard let placement = interstitialPlacement else {
            return
        }
        AATKit.stopPlacementAutoReload(placement)
    }
    
    @objc func didReceiveAd() {
        state = .loaded
    }
    
    @objc func loadedWithNoAds() {
        state = .loadedWithNoAds
    }
    
    func setupInterstitialPlacement() {
        state = .loading
        guard let placement = interstitialPlacement else {
            return
        }
        AATKit.setPlacementViewController(self, for: placement)
        AATKit.reload(placement, forceLoad: true)
    }
    
    @IBAction func loadInterstitial(_ sender: Any) {
        switch state {
        case .idle, .loadedWithNoAds:
            setupInterstitialPlacement()
        case .loading:
            print("loading")
        case .loaded:
            showInterstitialAd()
        }
    }
    
    func showInterstitialAd() {
        guard let placement = interstitialPlacement else {
            return
        }
        if AATKit.show(placement) {
            state = .idle
        } else {
            state = .loadedWithNoAds
        }
    }
    
    func updateView() {
        switch state {
        case .idle:
            loadButton.setTitle("Load Interstitial", for: .normal)
            loadButton.isEnabled = true
        case .loading:
            loadButton.setTitle("Loading...", for: .normal)
            loadButton.isEnabled = false
        case .loaded:
            loadButton.setTitle("Show Interstitial", for: .normal)
            loadButton.isEnabled = true
        case .loadedWithNoAds:
            loadButton.setTitle("No Ads, Try Again", for: .normal)
            loadButton.isEnabled = true
        }
    }
}
