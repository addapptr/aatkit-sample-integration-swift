//
//  AdsViewController.swift
//  AATKitSampleIntegration
//rewardedVideoSegue
//  Created by Mahmoud Amer on 11.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit

class AdsViewController: UIViewController {
    //Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //Banners Array
    var bannersDict: [String: UIView?] = ["fixedBanner": nil, "stickyBanner": nil]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AATKit.setViewController(self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
}

extension AdsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AdsRows.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case AdsRows.fixedBanner.rawValue:
            return cell(with: "InFeed Banner", indexPath: indexPath)
        case AdsRows.stickyBanner.rawValue:
            return cell(with: "Sticky Banner", indexPath: indexPath)
        case AdsRows.interstitial.rawValue:
            return clickableCell(indexPath: indexPath, name: "Interstitial Ad")
        case AdsRows.rewardedVideo.rawValue:
            return clickableCell(indexPath: indexPath, name: "Rewarded Video")
        case AdsRows.nativeAd.rawValue:
            return clickableCell(indexPath: indexPath, name: "Native Ad")
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case AdsRows.fixedBanner.rawValue:
            performSegue(withIdentifier: "InFeedSegue", sender: self)
            case AdsRows.stickyBanner.rawValue:
            performSegue(withIdentifier: "StickyBannerSegue", sender: self)
        case AdsRows.interstitial.rawValue:
            performSegue(withIdentifier: "InterstitialSegue", sender: self)
        case AdsRows.rewardedVideo.rawValue:
            performSegue(withIdentifier: "RewardedVideoSegue", sender: self)
        case AdsRows.nativeAd.rawValue:
            performSegue(withIdentifier: "nativeAdSegue", sender: self)
        default:
            print("Unknown")
        }
    }
    
    func cell(with title: String, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellsIdentifiers.embeddedBannerAd,
                                                       for: indexPath) as? EmbeddedAdTableViewCell else {
            return UITableViewCell()
        }
        cell.textLabel?.text = title
        return cell
    }
    
    func clickableCell(indexPath: IndexPath, name: String) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellsIdentifiers.embeddedBannerAd,
                                                       for: indexPath) as? EmbeddedAdTableViewCell else {
            return UITableViewCell()
        }
        cell.textLabel?.text = name
        return cell
    }
}
