//
//  AATKitConstants.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 06.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import Foundation

class Constants {
    static let aatAccountId: NSNumber = 154
    static let aatDebugEnabled = false
    
    struct CellsIdentifiers {
        static let embeddedBannerAd = "EmbeddedCell"
        static let clickableAd = "ClickableCell"
    }
}

enum PlacementsNames: String {
    case InFeedBannerPlacement
    case MultiSizeBanner
    case StickyBannerPlacement1
    case RewardedVideoPlacement1
    case InterstitialPlacement
    case NativeAdPlacement1
}

enum NotificationsNames: String {
    case aatKitHaveAd
    case aatKitUserEarnedIncentive
    case aatKitNoAds
}

enum AdsRows: Int {
    case fixedBanner
    case stickyBanner
    case interstitial
    case rewardedVideo
    case nativeAd
    case count
}
