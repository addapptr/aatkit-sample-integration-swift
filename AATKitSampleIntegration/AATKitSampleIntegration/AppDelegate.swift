//
//  AppDelegate.swift
//  AATKitSampleIntegration
//
//  Created by Mahmoud Amer on 05.11.19.
//  Copyright © 2019 Mahmoud Amer. All rights reserved.
//

import UIKit
import AATKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let locationManager = CLLocationManager.init()
    let cmp = AATCMPGoogle()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        locationManager.requestWhenInUseAuthorization()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "HomeNavigationController") as? HomeNavigationController else {
            return true
        }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = controller
        window?.makeKeyAndVisible()
        
        AATKit.debug(true)
        AATKit.initialize(with: aatKitConfiguration())
        return true
    }

}

extension AppDelegate: AATKitDelegate {
    func aatKitConfiguration() -> AATConfiguration {
        UserDefaults.standard.set(true, forKey: "gdpr_consent")
        
        let configuration = AATConfiguration()
        configuration.testModeAccountID = Constants.aatAccountId
        configuration.consentRequired = true
        let consent = AATManagedConsent(delegate: self, cmp: cmp)
        configuration.consent = consent
        configuration.delegate = self
        configuration.defaultViewController = nil
        return configuration
        
    }
    
    func aatKitHaveAd(_ placement: AATKitPlacement) {
        NotificationCenter.default.post(name: NSNotification.Name(NotificationsNames.aatKitHaveAd.rawValue), object: placement)
        print("👽👽👽 Placement \(placement) is populated with Ads.")
    }

    //Exclusively for havin an ad for a multi size banner
    func aatKitHaveAd(onMultiSizeBanner placement: AATKitPlacement, with placementView: UIView) {
        NotificationCenter.default.post(name: NSNotification.Name(NotificationsNames.aatKitHaveAd.rawValue), object: placement)
        print("👽👽👽 aatKitHaveAd \(placementView)")
    }

    //Exclusively for having watched a rewarded video
    func aatKitUserEarnedIncentive(on placement: AATKitPlacement) {
        NotificationCenter.default.post(name: NSNotification.Name(NotificationsNames.aatKitUserEarnedIncentive.rawValue), object: placement)
        print("👽👽👽 aatKitUserEarnedIncentive \(placement)")
    }

    func aatKitNoAds(_ placement: AATKitPlacement) {
        print("👽👽👽 aatKitNoAds \(placement)")
        NotificationCenter.default.post(name: NSNotification.Name(NotificationsNames.aatKitNoAds.rawValue), object: placement)
    }
    
    func aatKitPauseForAd() {
        print("👽👽👽 aatKitPauseForAd")
    }
    
    func aatKitResumeAfterAd() {
        print("👽👽👽 aatKitResumeAfterAd")
    }
}

extension AppDelegate: AATManagedConsentDelegate {
    func cmpNeedsUI(_ managedConsent: AATManagedConsent) {
        guard let vc = window?.rootViewController else {
            return
        }
        managedConsent.showIfNeeded(vc)
        /// CMP 1.0
        //        managedConsent.presentDialog(on: vc, completionHandler: nil)
    }

    func managedConsentCMPFinished(with state: AATManagedConsentState) {
        print("CMP FINISHED state: \(state)")
    }

    func managedConsentCMPFailed(toLoad managedConsent: AATManagedConsent, error string: String) {
        print("CMP LOAD faulire: \(string) ")
    }

    func managedConsentCMPFailed(toShow managedConsent: AATManagedConsent, error string: String) {
        print("CMP SHOW faulire: \(string) ")

    }
}
